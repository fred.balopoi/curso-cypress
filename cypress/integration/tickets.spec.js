describe("Tikets", () =>{
    const urlUso = "https://ticket-box.s3.eu-central-1.amazonaws.com/index.html";

    it("fills all the text input fields",() =>{
        const firstName = "Frederico";
        const lastName = "Vale";
        cy.visit(urlUso);
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("fred.balopoi@gmail.com");
        cy.get("#requests").type("Vegetariano pode ser uma boa ideia");
        cy.get("#signature").type(firstName+' '+ lastName);

    });

    it("selecionar valor 2",() =>{
        cy.visit(urlUso);
        cy.get("#ticket-quantity").select("2");
    });
    
    it("selecionar radio",()=> {
        cy.visit(urlUso);
        cy.get("#vip").check();
    });

    it("selecionar checkBox",()=>{
        cy.visit(urlUso);
        cy.get("#social-media").check();
    });

    
    it("selecionar checkBox e deselecionar depois",()=>{
        cy.visit(urlUso);
        cy.get("#social-media").check();
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#social-media").uncheck();
        cy.get("#friend").uncheck();
    });
    it("has 'TIKETBOX' header's heading", () =>{
        cy.visit(urlUso);
        cy.get("header h1").should("contain","TICKETBOX");
    });

    it("validação de classes com erro de preenchimento",()=>{
        cy.visit(urlUso);
        cy.get("#email")
        .as("email")
        .type("email_errado");
        
        cy.get("#email.invalid")
        .as("invalidoEmail")
        .should("exist");

        cy.get("@email")
        .clear()
        .type("email@valido.com");

        cy.get("#email.invalid").should("not.exist");
    });

    it("preenchimento completo ao final reset",() =>{
        const firstName = "Frederico";
        const lastName = "Vale";
        const fullName = firstName+' '+lastName;
        cy.visit(urlUso);
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email")
        .clear()
        .type("email@valido.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#publication").check();
        cy.get("#requests").type("Vegetariano pode ser uma boa ideia");
        cy.get(".agreement p").should(
            "contain",
            'I, '+fullName+', wish to buy 2 VIP tickets.'
        );
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        cy.get("button[type='submit']")
        .as("submitbutton")
        .should("not.be.disabled");
        cy.get("button[type='reset']").click();
        cy.get("@submitbutton").should("be.disabled");
    });

    it("uso de funções",()=>{
        const customer ={
            fistName: "João",
            lastName: "Silva",
            email:"joaosilva@gmail.com"
        };
        cy.visit(urlUso);
        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
        .as("submitbutton")
        .should("not.be.disabled");

        cy.get("#agree").uncheck();
        cy.get("@submitbutton").should("be.disabled");

    })



});